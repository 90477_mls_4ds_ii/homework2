# homework2


From the kaggle web service, download the [twitter sentiment dataset](https://www.kaggle.com/datasets/saurabhshahane/twitter-sentiment-dataset?resource=download).

Put the dataset in the `data` folder. 

Load the data with the `pd.read_csv()` function by using the zip file.

Clean data by using the available code in week2. In particular try to use code that is in the [zonato customers' text comments](https://gitlab.com/90477_mls_4ds_ii/week2/-/blob/main/use_cases/processing_zonato_customers_comments.ipynb) 
